const { sort } = require("./inventory");

module.exports = function(inventory)
{
    let result=[];
    for(let i=0;i<inventory.length;i++)
    {
        result.push(inventory[i].car_model);
    }
    return result.sort();
}
